# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-settings
pkgver=0_git20201117
pkgrel=0
_commit="4756c30da3af166b0846aa87e5cfd4b8ef9b006c"
pkgdesc="Settings application for Plasma Mobile"
arch="all !armhf"
url="https://community.kde.org/Plasma/Mobile"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	kded
	kirigami2
	qt5-qtquickcontrols2
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdelibs4support-dev
	ki18n-dev
	kio-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	solid-dev
	"
source="https://invent.kde.org/plasma-mobile/plasma-settings/-/archive/$_commit/plasma-settings-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=true ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="39b19238bf195df0e9f9d9e49e17bd99b32cb5256ff8715fe83a403c838072e2795cfc4464dcf8bfe823a959dfd930aff4156fd99b9d09f5fcc630cfe2b3dbfe  plasma-settings-4756c30da3af166b0846aa87e5cfd4b8ef9b006c.tar.gz"
